FROM alpine:3.11

RUN apk update && \
    apk --no-cache add mpd bash

RUN mkdir -p /usr/local/mpd/logs && \
    mkdir -p /usr/local/mpd/pids && \
    mkdir -p /usr/local/mpd/bin && \
    mkdir -p /usr/local/mpd/etc && \
    chown -R mpd /usr/local/mpd

ADD mpd.conf /usr/local/mpd/etc/mpd.conf
ADD start.sh /usr/local/mpd/bin/start.sh

RUN chmod +x /usr/local/mpd/bin/start.sh && \
    rm -rf /var/cache/apk/*

EXPOSE 6600 8000

ENTRYPOINT /usr/local/mpd/bin/start.sh