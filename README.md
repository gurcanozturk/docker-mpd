# docker-mpd

Music Play Daemon in docker

# Build it.
docker build -t docker-mpd .

# Run it.
docker run --name mpd -d -p 6680:6680 docker-mpd

# Connect it.
Use any MPD client to connect <docker_ip>:6680